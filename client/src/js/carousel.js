import slick from 'slick-carousel';
import $ from 'jquery'

$('.js-slider-2').slick({
	dots: true,
	arrows:false,
	infinite: true,
	speed: 300,
	slidesToShow: 6,
	slidesToScroll: 1,
	responsive: [
	  {
		 breakpoint: 1024,
		 settings: {
			slidesToShow: 4,
			slidesToScroll: 1,
		 }
	  },
	  {
		 breakpoint: 600,
		 settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		 }
	  },
	  {
		 breakpoint: 480,
		 settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		 }
	  },
	  {
		breakpoint: 320,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
		 }
	  }
	]
 });