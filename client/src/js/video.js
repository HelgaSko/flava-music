const play = [...document.getElementsByClassName('js-play')];
const heroImage = Array.from(document.getElementsByClassName('js-hero-bg'))[0];
const heroVideo = Array.from(document.getElementsByClassName('js-video'))[0];
const videoCover = Array.from(document.getElementsByClassName('js-cover'))[0];
play.forEach(function (button) {
	button.addEventListener('click', (e) => {
		button.classList.toggle('is-active');
		heroImage.classList.toggle('is-active');
		heroVideo.classList.toggle('is-active');
		videoCover.classList.toggle('is-active');
	})
})