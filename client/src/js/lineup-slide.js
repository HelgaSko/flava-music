import slick from 'slick-carousel';
import $ from 'jquery'

$('.js-slider-1').slick({
	dots: false,
	arrows:true,
	appendArrows: $('.slider__controls'),
	prevArrow: '<button type="button" class="arrows__prev slick-prev"><</button>',
	nextArrow: '<button type="button" class="arrows__next slick-next">></button>',
	infinite: false,
	speed: 300,
	slidesToShow: 3,
	slidesToScroll: 1,
	responsive: [
	  {
		 breakpoint: 992,
		 settings: {
			slidesToShow: 2,
			slidesToScroll: 1,
		 }
	  },
	  {
		 breakpoint: 576,
		 settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		 }
	  }
	  // You can unslick at a given breakpoint now by adding:
	  // settings: "unslick"
	  // instead of a settings object
	]
 });