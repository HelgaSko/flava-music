const burgers = [...document.getElementsByClassName('js-burger')];
const logo = Array.from(document.getElementsByClassName('js-logo'))[0];
const burgerMenu = Array.from(document.getElementsByClassName('js-navigation__list'))[0];
burgers.forEach(function (burger) {
	burger.addEventListener('click', (e) => {
	burger.classList.toggle('is-active');
	logo.classList.toggle('is-active');
	burgerMenu.classList.toggle('is-active');
	})
})